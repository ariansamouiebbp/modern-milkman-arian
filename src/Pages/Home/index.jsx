import React, {useEffect} from 'react';
import {Container, Row, Col} from 'reactstrap';
import Axios from 'axios';
import Loading from '../../Components/Elements/Loading';
import CategoryList from '../../Components/CategoryList';
import CategoryItem from '../../Components/CategoryItem';
import ItemMessage from '../../Components/ItemMessage';

const Home = () => {

    // States
    const [loading, setLoading] = React.useState(true);
    const [loadingItem, setLoadingItem] = React.useState(null);
    const [pageData, setPageData] = React.useState([]);
    const [dataActive, setDataActive] = React.useState({});
    const [selectAllState, setSelectAllState] = React.useState(false);

    // Functions
    const getPageData = () => {
        setLoading(true);
        Axios.get('https://mm-react-fe-interview-task.herokuapp.com/categoryProducts')
            .then(response => {
                // console.log(response);
                setPageData(response.data);
                setDataActive(response.data[0]);
                setLoading(false);
            })
            .catch(error => {
                console.log(error);
            });
    };

    const selectCategory = (item) => {
        setLoadingItem(true);
        setDataActive(item);
        setTimeout(() => {
            setLoadingItem(false);
        }, 100);
    };

    useEffect(() => {
        getPageData();
    }, []);

    return (<>
        {loading && <Loading/>}
        {!loading && <Container className="page-container">
            <Row>
                <Col xs={12} md={3}>
                    <CategoryList
                        data={pageData}
                        onClick={selectCategory}
                        activeState={dataActive}
                        setDataActive={setDataActive}
                        selectAllState={selectAllState}
                        setSelectAllState={setSelectAllState}
                    />
                </Col>
                <Col xs={12} md={9}>
                    {!loadingItem && Object.keys(dataActive).length !== 0 && <CategoryItem data={dataActive}/>}
                    {!loadingItem && Object.keys(dataActive).length === 0 && !selectAllState && <ItemMessage message="PLEASE SELECT A CATEGORY"/>}
                    {!loadingItem && selectAllState && pageData.map((item, key) => <CategoryItem data={item} key={`select-all-category-item-key-${key}`}/>)}
                </Col>
            </Row>
        </Container>}
    </>);
};

export default Home;
