import React from 'react';
import './App.scss';
import {Route, BrowserRouter as Router, Switch} from 'react-router-dom';

import Home from './Pages/Home';

const App = () => {
    return (<div className="App">
        <Router>
            <Switch>
                <Route path="/" exact component={Home}/>
            </Switch>
        </Router>
    </div>);
};

export default App;
