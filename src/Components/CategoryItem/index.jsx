import React, {useState, useEffect} from 'react';
import {Row, Col} from 'reactstrap';
import CategoryInfoCard from '../CategoryInfoCard';

const CategoryItem = (props) => {

    // data
    const {data} = props;

    // States
    const [image, setImage] = useState('');

    useEffect(() => {
        data.title === 'Milk' ? setImage('images/milk.png') :
        data.title === 'Milkshakes' ? setImage('images/milkshakes.png') :
        data.title === 'Fruit Juice' ? setImage('images/fruitjuice.png') :
        data.title === 'Eggs' ? setImage('images/eggs.png') :
        data.title === 'Creamery' ? setImage('images/butter.png') :
        data.title === 'Christmas' ? setImage('images/christmas.png') :
        data.title === 'Bakery' ? setImage('images/bakery.png') :
        data.title === 'Spring Water' ? setImage('images/water.png') :
        data.title === 'Soft Drinks' ? setImage('images/soft-drinks.png') :
        data.title === 'Veg Boxes' ? setImage('images/veg-box.png') :
            setImage('')
    },[data]);

    return (<div className="category-item">
        <div className="item-head">
            <h1 className="item-title">{data.title}</h1>
            <img src={image} alt={`thumbnail - ${data.title}`} className="item-image"/>
        </div>
        <Row className="item-card-row">
            {data.data.map((item, key) =>
                <Col xs={12} sm={6} md={3} key={`item-info-key-${key}`} className="item-card-wrapper">
                    <CategoryInfoCard data={item} />
                </Col>
            )}
        </Row>
    </div>);
};

export default CategoryItem;
