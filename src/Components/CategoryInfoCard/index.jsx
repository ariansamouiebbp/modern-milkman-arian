import React, {useState, useEffect} from 'react';

const CategoryInfoCard = (props) => {

    // data
    const {data} = props;

    // States
    const [stockText, setStockText] = useState('');
    const [outOfStock, setOutOfStock] = useState(false);

    const checkStock = () => {
        if (data.stock > 0 && data.stock < 11) {
            setStockText('Few Left');
        }
        if (data.stock === 0) {
            setOutOfStock(true);
            setStockText('Out of Stock');
        }
        if (data.stock > 10) {
            setStockText('In Stock');
        }
    };

    useEffect(() => {
        checkStock();
    },[]);

    return (<div className="category-info-card">
        <p className="info-text dark title">{data.title}</p>
        <p className="info-text">£{data.price} {data.amount}</p>
        <p className={`info-text ${outOfStock ? 'error' : ''}`}>
            <span className="stock">{stockText}:</span> {data.stock}
        </p>
    </div>);
};

export default CategoryInfoCard;
