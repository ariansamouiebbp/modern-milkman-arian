import React, {useState} from 'react';

const CategoryList = (props) => {

    // Data
    const {data, onClick, activeState, setDataActive, selectAllState, setSelectAllState} = props;

    // States
    const [navState, setNavState] = useState(false);

    // Functions
    const selectItem = (item) => {
        onClick(item);
        setNavState(false);
        setSelectAllState(false);
    };

    const clearFilter = () => {
        setDataActive({});
        setNavState(false);
        setSelectAllState(false);
    };

    const handleSelectAll = () => {
        setDataActive({});
        setNavState(false);
        setSelectAllState(true);
    };

    return (<div className={`category-list-wrapper ${navState ? 'active' : ''}`}>
        <ul className="category-list">
            {data.map((item, key) => <li className="list-item" key={`category-list-key-${key}`}>
                <button className={`item-button ${activeState.title === item.title ? 'active' : ''}`}
                        onClick={() => selectItem(item)}>{item.title}</button>
            </li>)}
            <li className="list-item">
                <button className={`item-button ${selectAllState ? 'active' : ''}`} onClick={handleSelectAll}>All Categories</button>
            </li>
            <li className="list-item">
                <button className="clickable" onClick={clearFilter}>CLEAR</button>
            </li>
        </ul>
        <button
            className="close-button"
            onClick={() => setNavState(!navState)}
        >{navState ? 'CLOSE' : 'CATEGORIES'}</button>
    </div>);
};

export default CategoryList;
