import React from 'react';

const ItemMessage = (props) => {

    // Data
    const {message} = props;

    return (<div className="item-message">
        {message}
    </div>);
};

export default ItemMessage;
